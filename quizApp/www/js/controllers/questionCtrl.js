angular.module('starter')

.controller('questionCtrl', questionCtrl);
function questionCtrl(currentQuiz,$state,$ionicPopup,$scope,$ionicModal,$window,backButtonForQuestions){
  var question = this;
  question.filteredQuestions=currentQuiz.filteredQuestions;
    console.log(question.filteredQuestions);

    //set review in center
    var devWidth = $window.innerWidth;
    question.centrElement = {'margin-right': ((devWidth-102)*40)/100  +'px'}
    question.centrSubmit = {'margin-right': ((devWidth-92)*40)/100  +'px'}
    console.log($window.innerWidth);
    console.log(question.centrElement);


    question.index=0;

    question.totalQuestions=question.filteredQuestions.length;
    question.timeInSecond=question.filteredQuestions.length*60;

    question.showQuestion=function(index,a){
        if(a==1){
            if(question.index>1){
                question.index--;

            }

            question.appear= question.filteredQuestions[question.index-1];
            question.isArrayCheck=Array.isArray(question.appear.correct);


        }
        else{
            question.appear=question.filteredQuestions[question.index];
            question.isArrayCheck=Array.isArray(question.appear.correct);
            console.log(question.appear);

            if(question.index<question.totalQuestions) {
                question.index++;

            }

        }

    }

       question.selectedChange=function(optionId){


    }

    question.choice=function(optionId){
        question.appear.answer=optionId;


    }

        //apply for multiple selected answer
    question.arrayChoice=function(optionId){
        //question.appear.answer=[];
        if(!question.appear.hasOwnProperty('answer'))
        {
            question.appear.answer=[];
        }

         var indexValue=question.appear.answer.indexOf(optionId);

        if(indexValue!=null&&indexValue!=-1) {
            question.appear.answer.splice(indexValue, 1);
            if (question.appear.answer.length == 0)
                delete question.appear.answer;
        }
        else{
            question.appear.answer.push(optionId);
        }



    }

    $ionicModal.fromTemplateUrl('templates/review.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });


    question.GoToReview=function(){
        $scope.modal.show();

        //$state.go('review');

    }



    question.finished=function(){

        $scope.showAlert = function() {
            var alertPopup = $ionicPopup.alert({
                title: 'Time Over',
                template: 'Sorry you have run out of time for this quiz'
            });
        }
        $scope.showAlert();
        question.filteredQuestions=null;
        $scope.modal.hide();
        $state.go('results',{status:1});

    }


  //TODO
  //
    question.forceClose=function(){
        $scope.showConfirm = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Quiz Close',
                template: 'This will end the quiz without saving. Do you want to continue'
            }).then(function(res) {
                if(res) {
                    console.log('You are sure');
                    $state.go('home');

                } else {
                    console.log('You are not sure');
                }
            });
        };
        currentQuiz.filteredQuestions=null;
        currentQuiz.type=null;
        currentQuiz.paper=null;
        $scope.showConfirm();

    }
    console.log($scope.QuestionId);
    //show particular question when click on a question in review modal

    $scope.reviewSelect=function(index){
        question.index=index;
        console.log(question.index);
        question.appear=question.filteredQuestions[question.index-1];
        question.isArrayCheck=Array.isArray(question.appear.correct);
        $scope.modal.hide();
    }

    //on backButton pressed
    backButtonForQuestions.backcallfun();
}
  
