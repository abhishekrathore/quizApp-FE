// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic','ngCordova','LocalForageModule','timer'])

  .run(function($ionicPlatform, $localForage, $rootScope) {
    // $localForage.clear();
    $ionicPlatform.ready(function() {
      if(window.cordova && window.cordova.plugins.Keyboard) {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

        // Don't remove this line unless you know what you are doing. It stops the viewport
        // from snapping when text inputs are focused. Ionic handles this internally for
        // a much nicer keyboard experience.
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if(window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  })
  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
        $ionicConfigProvider.views.maxCache(0);
        if( ionic.Platform.isAndroid() ) {
            $ionicConfigProvider.scrolling.jsScrolling(false);
        }
    // $ionicConfigProvider.views.transition('none');
    // $ionicConfigProvider.backButton.text('').icon('ion-ios-arrow-left').previousTitleText(false);

    $stateProvider
      .state('home', {
        url: "/home",
        templateUrl: "templates/home.html",
        controller:'homeCtrl',
        controllerAs: 'home'
      })
      .state('quizDetail', {
        url: "/quizDetail",
        templateUrl: "templates/quizDetail.html",
        controller:'quizDetailCtrl',
        controllerAs: 'detail',
            params:{
                topic:null,
                paper:null
            }
      })
      .state('question', {
        url: "/question",
        templateUrl: "templates/question.html",
        controller:'questionCtrl',
        controllerAs: 'question'
      })
        .state('checkAnswer', {
            url: "/checkAnswer",
            templateUrl: "templates/checkAnswer.html",
            controller:'checkAnswerCtrl',
            controllerAs: 'checkAnswer',
            params:{
                key:null,
                history:null
            }
        })
      .state('results', {
          url:"/results",
          templateUrl: "templates/results.html",
          controller:'resultsCtrl',
          controllerAs: 'results',
            params:{
                status:null

            }
      });

    $urlRouterProvider.otherwise('/home');

  })
  .constant('backend','http://localhost');
  // .constant('backend','http://ec2-54-70-159-179.us-west-2.compute.amazonaws.com:8080')
